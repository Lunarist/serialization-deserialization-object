﻿using System;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

namespace Serialization_Deserialization
{
    [Serializable]
    class Vehicle
    {
        public string name;
        public int wheel;

        public Vehicle(string _name, int _wheel)
        {
            name = _name;
            wheel = _wheel;
        }

        public virtual void Describe()
        {
            
        }
    }

    [Serializable]
    class Bus : Vehicle
    {
        public int capacity;

        public Bus(string _name, int _wheel, int _capacity) : base(_name, _wheel)
        {
            capacity = _capacity;
        }

        public override void Describe()
        {
            Console.WriteLine(name + "has " + wheel.ToString() + "wheels with " + capacity.ToString() + "capacity");
        }
    }

    [Serializable]
    class Truck : Vehicle
    {
        public double volume;

        public Truck(string _name, int _wheel, int _volume) : base(_name, _wheel)
        {
            volume = _volume;
        }

        public override void Describe()
        {
            Console.WriteLine(name + "has " + wheel.ToString() + "wheels with " + volume.ToString() + "volume");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Initialize Object for serialization
            Bus bus = new Bus("Sumber Kencono", 8, 50);
            Truck truck = new Truck("Fuso", 6, 100);

            Console.WriteLine("Serialization");

            byte[] dataBus = Serialization(bus);
            byte[] dataTruck = Serialization(truck);

            OutputByte(dataBus);
            OutputByte(dataTruck);

            Console.WriteLine("\n Deserialization");

            Vehicle objBus = Deserialization(dataBus);
            Vehicle objTruck = Deserialization(dataTruck);

            OutputObject(objBus);
            OutputObject(objTruck);
        }

        static byte[] Serialization(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        static byte[] Deserialization(byte[] arrBytes)
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                ms.Write(arrBytes, 0, arrBytes.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var obj = bf.Deserialize(ms);
                return (Vehicle)obj;
            }
        }

        static void OutputObject(Vehicle _vehicle)
        {
            _vehicle.Describe();
        }

        static void OutputByte(byte[] vehicleBytes)
        {
            Console.WriteLine("new byte[] { " + string.Join(", ", vehicleBytes) + " }");
        }
    }
}
